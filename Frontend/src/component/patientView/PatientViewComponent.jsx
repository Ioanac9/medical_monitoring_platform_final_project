import React, { Component } from 'react'
import ApiServiceMedication from "../../service/ApiServiceMedication";
import ApiServicePatient from "../../service/ApiServicePatient";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import ApiServicePrescription from "../../service/ApiServicePrescription";


class ListMedicationComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            patients: [],
            prescription: '',
            id:'',
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            medicalRecord: '',
            medications:[],
            intokeInterval:'',
            periodTreatment:'',
            message: null
        }




        this.editPatient = this.editPatient.bind(this);
        this.reloadPatientList = this.reloadPatientList.bind(this);
        this.reloadPrescriptionList = this.reloadPrescriptionList.bind(this);

    }




    componentDidMount() {
        this.reloadPrescriptionList();
        this.reloadPatientList();
    }

    reloadPatientList() {
        ApiServicePatient.fetchPatientById(window.localStorage.getItem("idPatientLogin"))
            .then((res) => {
                let patient = res.data.result;
                this.setState({
                    id: patient.id,
                    firstName: patient.firstName,
                    lastName: patient.lastName,
                    birthDate: patient.birthDate,
                    gender: patient.gender,
                    address:patient.address,
                    medicalRecord: patient.medicalRecord,
                    medications: patient.medications

                })

            });
    }

    reloadPrescriptionList() {
        ApiServiceMedication.fetchMedicationById(window.localStorage.getItem("medicationIdForPrescription"))
            .then((res) => {
                let p = res.data.result;
                this.setState({prescription: res.data.result,
                    intokeInterval :p.intokeInterval,
                    periodTreatment: p.periodTreatment

                })
            });
    }



    editPatient(id) {
        window.localStorage.setItem("patientId", id);
        this.props.history.push('/edit-patient-view');
    }


    render() {
        return (
            <div>

                <Typography variant="h4" style={style}>Patient Account Details</Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align = "right" >First Name</TableCell>
                            <TableCell align = "right" >Last Name</TableCell>
                            <TableCell align="right">Birth Date</TableCell>
                            <TableCell align="right">Gender</TableCell>
                            <TableCell align="right">Address</TableCell>
                            <TableCell align="right">Medical Records</TableCell>

                        </TableRow>

                    </TableHead>
                    <TableBody>

                                <TableCell align="right">{this.state.firstName}</TableCell>
                                <TableCell align="right">{this.state.lastName}</TableCell>
                                <TableCell align="right">{this.state.birthDate}</TableCell>
                                <TableCell align="right">{this.state.gender}</TableCell>
                                <TableCell align="right">{this.state.address}</TableCell>
                                <TableCell align="right">{this.state.medicalRecord}</TableCell>

                                <TableCell align="right" onClick={() => this.editPatient(this.state.id)}><CreateIcon /></TableCell>


                    </TableBody>
                </Table>

                <Typography variant="h4" style={style}> Medication Plan Details</Typography>
                <Table>

                    <TableBody>

                        <TableCell >
                            <TableRow>
                                <TableCell align="right">Medicament Id</TableCell>
                                <TableCell align="right">Medicament Name</TableCell>
                                <TableCell align="right">Side Effects</TableCell>
                                <TableCell align="right">Dosage</TableCell>
                                <TableCell align="right">Intoke Interval</TableCell>
                                <TableCell align="right">Period Treatment</TableCell>



                            </TableRow>
                            {this.state.medications.map(row => {
                            return (

                                <TableRow key={row.id}>

                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell >{row.name}</TableCell>
                                    <TableCell align="right">{row.sideEffects}</TableCell>
                                    <TableCell align="right">{row.dosage}</TableCell>

                                    <TableCell align="right">{row.prescription.intokeInterval}</TableCell>
                                    <TableCell align="right">{row.prescription.periodTreatment}</TableCell>


                                </TableRow>
                            )})}



                        </TableCell>


                    </TableBody>

                </Table>

            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListMedicationComponent;