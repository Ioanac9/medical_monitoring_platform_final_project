import React, { Component } from 'react'
import ApiServicePatient from "../../service/ApiServicePatient";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import CreateIcon from "@material-ui/core/SvgIcon/SvgIcon";
import ApiServiceMedicationPlan from "../../service/ApiServiceMedicationPlan";

class AddMedicationPlanComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            id:'',
            firstName:'',
            lastName:'',
            birthDate:'',
            gender: '',
            address: '',
            medicalRecord:'',
            message: null
        }
        this.saveMedicationPlan = this.saveMedicationPlan.bind(this);
        this.loadPatient = this.loadPatient.bind(this);


    }

    componentDidMount() {
        this.loadPatient();

    }

    loadPatient() {
        ApiServicePatient.fetchPatientById(window.localStorage.getItem("medicationPlanPatientId"))
            .then((res) => {
                let patient= res.data.result;
                this.setState({
                    id:patient.id,
                    firstName:patient.firstName,
                    lastName:patient.lastName,
                    birthDate: patient.birthDate,
                    gender: patient.gender,
                    address: patient.address,
                    medicalRecord: patient.medicalRecord

                })

            });
    }


    saveMedicationPlan () {

        let medicationPlan ={patientId :window.localStorage.getItem("medicationPlanPatientId"),
            patientFirstName:'',
            patientLastName:'',
            patientGender:'',
            patientAddress:''};

        ApiServiceMedicationPlan.addMedicationPlan(medicationPlan)
            .then(res => {
                this.setState({message : 'Medication plan added successfully.'});
                this.props.history.push('/list-medication-plan');
            });


    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });


    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Medical Plan</Typography>
                <form style={formContainer}>

                    <Button variant="contained" color="primary" onClick={this.saveMedicationPlan}>Save</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddMedicationPlanComponent;