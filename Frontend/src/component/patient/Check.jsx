import React, { Component } from 'react'
import ApiServicePatient from "../../service/ApiServicePatient";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";

const caregiver ={
    id : window.localStorage.getItem("caregiverId"),
    firstName : window.localStorage.getItem("caregiverFirstName"),
    lastName: '',
    birthDate:'',
    gender: '',
    address:''}

localStorage.setItem('caregiver',JSON.stringify(caregiver));
class Check extends Component{

    constructor(props){
        super(props);
        this.state ={
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            medicalRecord: '',
            caregiverId :'',
            caregiverFirstName:'',
            caregiverLastName:'',
            caregiverGender:'',
            caregiverAddress:'',

            message: null
        }
        this.savePatient = this.savePatient.bind(this);
    }


    savePatient(){
      //  e.preventDefault();

      //  {id: window.localStorage.getItem("caregiverId"),firstName: window.localStorage.getItem("caregiverFirstName")} ,
        let patient = {firstName: this.state.firstName, lastName: this.state.lastName, birthDate: this.state.birthDate, gender: this.state.gender,
                       address: this.state.address, medicalRecord: this.state.medicalRecord,caregiverId :window.localStorage.getItem("caregiverId"),
            caregiverFirstName:'',
            caregiverLastName:'',
            caregiverGender:'',
            caregiverAddress:''};
        ApiServicePatient.addPatient(patient)
            .then(res => {
                this.setState({message : 'Patient added successfully.'});
                this.props.history.push('/patients');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Patient PLEASEEE</Typography>
                <form style={formContainer}>

                    <TextField type="text" placeholder="First Name" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Last Name" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange}/>

                    <TextField type="date" placeholder="Birth Date" fullWidth margin="normal" name="birthDate" value={this.state.birthDate} onChange={this.onChange}/>

                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Male" onChange={this.onChange}/>Male
                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Female" onChange={this.onChange}/>Female

                    <TextField placeholder="Address" fullWidth margin="normal" name="address" value={this.state.address} onChange={this.onChange}/>

                    <TextField placeholder="Medical Record" fullWidth margin="normal" name="medicalRecord" value={this.state.medicalRecord} onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.savePatient}>Save</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default Check;