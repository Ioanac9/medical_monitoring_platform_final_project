import React, { Component } from 'react'
import ApiServicePatient from "../../service/ApiServicePatient";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radio from "@material-ui/core/Radio";

class EditPatientComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            medicalRecord: '',
            message: null
        }
        this.savePatient = this.savePatient.bind(this);
        this.loadPatient = this.loadPatient.bind(this);
    }

    componentDidMount() {
        this.loadPatient();
    }

    loadPatient() {
        ApiServicePatient.fetchPatientById(window.localStorage.getItem("patientId"))
            .then((res) => {
                let patient = res.data.result;
                this.setState({
                    id: patient.id,
                    firstName: patient.firstName,
                    lastName: patient.lastName,
                    birthDate: patient.birthDate,
                    gender: patient.gender,
                    medicalRecord: patient.medicalRecord
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    savePatient = (e) => {
        e.preventDefault();
        let patient = {id: this.state.id,
            firstName: this.state.firstName,lastName: this.state.lastName , birthDate: this.state.birthDate,
            gender: this.state.gender, address: this.state.address, medicalRecord: this.state.medicalRecord};
        ApiServicePatient.editPatient(patient)
            .then(res => {
                this.setState({message : 'Patient added successfully.'});
                this.props.history.push('/patient-view');
            });
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Edit Patient</Typography>
                <form>


                    <TextField type="text" placeholder="First Name" fullWidth margin="normal" name="firstName" value={this.state.firstName} onChange={this.onChange}/>
                    <TextField type="text" placeholder="Last Name" fullWidth margin="normal" name="lastName" value={this.state.lastName} onChange={this.onChange}/>

                    <TextField type="date" placeholder="Birth Date" fullWidth margin="normal" name="birthDate" value={this.state.birthDate} onChange={this.onChange}/>

                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Male" onChange={this.onChange}/>Male
                    <Radio type = "radio" placeholder="Gender" fullWidth margin="normal" name="gender" value= "Female" onChange={this.onChange}/>Female

                    <TextField placeholder="Address" fullWidth margin="normal" name="address" value={this.state.address} onChange={this.onChange}/>

                    <TextField placeholder="Medical Record" fullWidth margin="normal" name="medicalRecord" value={this.state.medicalRecord} onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.savePatient}>Edit</Button>

                </form>
            </div>
        );
    }
}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default EditPatientComponent;