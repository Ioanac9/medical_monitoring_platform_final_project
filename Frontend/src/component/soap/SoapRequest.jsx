 import React, {Component} from 'react';

 class SoapRequest extends Component{
     constructor(props){
         super(props)
         this.state={result:''};
         this.callWebService = this.callWebService.bind(this);
     }

     callWebService()
     {
         var xmlhttp = new XMLHttpRequest();
         xmlhttp.open('POST','http://localhost:8080/ws',true);

         var sr ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:my="http://company.com/myloyal">\n' +
             '    <soapenv:Header/>\n' +
             '    <soapenv:Body>\n' +
             '        <my:retrieveCustomerLoyaltyPoints>\n' +
             '            <my:cin>1</my:cin>\n' +
             '        </my:retrieveCustomerLoyaltyPoints>\n' +
             '    </soapenv:Body>\n' +
             '</soapenv:Envelope>';
         xmlhttp.onreadystatechange = function(){
             if(xmlhttp.readyState ==4){
                 if(xmlhttp.status ==200){
                     alert('Response:'+xmlhttp.response);
                     this.setState({result :  xmlhttp.response.text});

                 }
             }
         }

         let headers = new Headers();

         headers.append('Content-Type', 'text/xml');
         headers.append('Accept', 'application/json');

         headers.append('Access-Control-Allow-Origin', 'http://localhost:3000');
         headers.append('Access-Control-Allow-Credentials', 'true');


         xmlhttp.setRequestHeader('Content-Type','text/xml');
         xmlhttp.send(sr);

     }
     render(){
         return(
             <div>

                 <input type="button" onClick={this.callWebService} value = "Call SOAP WebService"/>
             </div>
         );
     }
 }
 export default SoapRequest;