import React, { Component } from 'react';


import Chart from './Chart';
import ApiServiceActivities from "../../service/ApiServiceActivities";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Button from "@material-ui/core/Button";


class MainChart extends Component {

    constructor(props) {
        super(props)
        this.state = {
            chartData:{},
            activities: [],
            notOkActivities: [],
            observations:'',
            message: null
        }

        this.reloadActivitiesList = this.reloadActivitiesList.bind(this);
        this.reloadNotOkActivitiesList = this.reloadNotOkActivitiesList.bind(this);
    }

    componentDidMount() {
        this.reloadActivitiesList();
        this.reloadNotOkActivitiesList();
    }

    reloadActivitiesList() {
        ApiServiceActivities.fetchActivities()
            .then((res) => {
                this.setState({activities: res.data.result})
            });
    }

    reloadNotOkActivitiesList() {
        ApiServiceActivities.fetchNotOkActivities()
            .then((res) => {
                this.setState({notOkActivities: res.data.result})
            });
    }

    addRecomandation(id){
        window.localStorage.setItem("patientIdChart", id);
        this.props.history.push('/edit-recomandation');

    }
    addNoRecomandation(id){
        this.props.history.push('/chart');

    }

    getChartData(){
        // Ajax calls here

        this.setState({
            chartData:{
                labels: ["Showering","Toileting","Grooming","Spare Time/TV","Breakfast","Sleeping","Lunch","Leaving"],
                datasets:[
                    {
                        label:'Times',
                        data:[
                            15,
                            45,
                            30,
                            78,
                            6,
                            14,
                            10,
                            14
                        ],
                        backgroundColor:[
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)'
                        ]
                    }
                ]
            }
        });
    }

    componentWillMount(){

        this.getChartData();
    }


    render() {
        return (
            <div className="App">

                <Chart chartData={this.state.chartData} location="Massachusetts" legendPosition="bottom"/>
                <br></br>
                <Table>
                </Table>
                <br></br>
                <Typography variant="h4" >Anormal activities</Typography>


                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Activity Id</TableCell>
                            <TableCell align="right">Start Time</TableCell>
                            <TableCell align="right">End Time</TableCell>
                            <TableCell align="right">Label</TableCell>
                            <TableCell align="right">Observations</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.notOkActivities.map(row => (

                            <TableRow key={row.id }>
                                <TableCell component="th" scope="row">
                                    {row.id}

                                </TableCell>
                                <TableCell align="right">{row.startTime}</TableCell>
                                <TableCell align="right">{row.endTime}</TableCell>
                                <TableCell align="right">{row.label}</TableCell>
                                <TableCell align="right">{row.observations}</TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <br></br>

                <Button variant="contained" color="secondary" onClick={() => this.addRecomandation("1")}>
                    Set Anormal Comportment
                </Button>
                <Button variant="contained" color="primary" onClick={() => this.addNoRecomandation()}>
                    Set Normal Comportment
                </Button>

                <Table>
                </Table>
                <br></br>
                <br></br>
                <br></br>



            </div>
        );
    }
}

export default MainChart;
