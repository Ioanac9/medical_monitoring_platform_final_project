import React, { Component } from 'react'
import ApiPrescriptionStatus from "../../service/ApiPrescriptionStatus";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';

class PrescriptionStatusView extends Component {


    constructor(props) {
        super(props)
        this.state = {
            medications: [],
            tokenPill:' ',
            message: null
        }

        this.reloadMedicationList = this.reloadMedicationList.bind(this);
    }

    componentDidMount() {
        this.reloadMedicationList();
    }

    reloadMedicationList() {
        ApiPrescriptionStatus.fetchStatus()
            .then((res) => {
                this.setState({medications: res.data.result})
            });
    }


    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Prescriptions Status</Typography>


                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Medication Id</TableCell>
                            <TableCell align="right">Name</TableCell>
                            <TableCell align="right">Side Effects</TableCell>
                            <TableCell align="right">Dosage</TableCell>
                            <TableCell align="right">Intoke Interval</TableCell>
                            <TableCell align="right">Period</TableCell>
                            <TableCell align="right">Token Pill Details</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.medications.map(row => (
                            <TableRow key={row.medicationId}>
                                <TableCell component="th" scope="row">
                                    {row.medicationId}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.sideEffects}</TableCell>
                                <TableCell align="right">{row.dosage}</TableCell>
                                <TableCell align="right">{row.intokeInterval}</TableCell>
                                <TableCell align="right">{row.periodTreatment}</TableCell>
                                <TableCell align="right">{row.tokenPill}</TableCell>


                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </div>
        );
    }

}


const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default PrescriptionStatusView;