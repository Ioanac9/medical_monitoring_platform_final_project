import React, { Component } from 'react'
import ApiServiceMedication from "../../service/ApiServiceMedication";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Input} from "@material-ui/icons";
import Radio from "@material-ui/core/Radio";
import ApiServiceRecomandation from "../../service/ApiServiceRecomandation";

class EditRecomandation extends Component{

    constructor(props){
        super(props);
        this.state ={
           recomandation:'',
            message: null
        }
        this.saveMedication = this.saveMedication.bind(this);
    }

    saveMedication = (e) => {
        e.preventDefault();
        let recomandation = {recomandation: this.state.recomandation,patient_id: window.localStorage.getItem("patientIdChart")};
        // ApiServiceRecomandation.addRecomandation(recomandation)
        //     .then(res => {
        //         this.setState({message : 'Medication added successfully.'});
        //         this.props.history.push('/doctors');
        //     });
        window.localStorage.setItem("recomandationFromDoctor",this.state.recomandation)
        this.props.history.push('/chart');
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add Recomandation</Typography>
                <form style={formContainer}>

                    <TextField type="text" placeholder="recomandation" fullWidth margin="normal" name="recomandation" value={this.state.recomandation} onChange={this.onChange}/>
                    <Button variant="contained" color="primary" onClick={this.saveMedication}>Save</Button>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default EditRecomandation;