import React, { Component } from 'react'
import ApiServiceCaregiver from "../../service/ApiServiceCaregiver";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radio from "@material-ui/core/Radio";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import CreateIcon from "@material-ui/core/SvgIcon/SvgIcon";
import ApiServicePatient from "../../service/ApiServicePatient";

class CaregiverViewComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
            id: '',
            firstName: '',
            lastName: '',
            birthDate: '',
            gender: '',
            address: '',
            patients: [],
            message: null
        }

        this.loadCaregiver = this.loadCaregiver.bind(this);

    }

    componentDidMount() {
        this.loadCaregiver();

    }

    reloadPatientList() {
        ApiServicePatient.fetchPatients()
            .then((res) => {
                this.setState({patients: res.data.result})
            });
    }

    loadCaregiver() {
        ApiServiceCaregiver.fetchCaregiverById(window.localStorage.getItem("idCaregiverLogin"))
            .then((res) => {
                let caregiver = res.data.result;
                this.setState({
                    id: caregiver.id,
                    firstName: caregiver.firstName,
                    lastName: caregiver.lastName,
                    birthDate: caregiver.birthDate,
                    gender: caregiver.gender,
                    medicalRecord: caregiver.medicalRecord,
                    patients: caregiver.patients

                })
            });
    }


    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>My Account Details</Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Id</TableCell>
                            <TableCell align="right">First Name</TableCell>
                            <TableCell align="right">Last Name</TableCell>
                            <TableCell align="right">Birth Date</TableCell>
                            <TableCell align="right">Gender</TableCell>
                            <TableCell align="right">Address</TableCell>
                            <TableCell align="right">Patients</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableCell align="right">{this.state.id}</TableCell>
                        <TableCell align="right">{this.state.firstName}</TableCell>
                        <TableCell align="right">{this.state.lastName}</TableCell>
                        <TableCell align="right">{this.state.birthDate}</TableCell>
                        <TableCell align="right">{this.state.gender}</TableCell>
                        <TableCell align="right">{this.state.address}</TableCell>

                    </TableBody>
                </Table>

                <Typography variant="h4" style={style}>My Associated Patients</Typography>

                <Table>

                    <TableBody>
                        <TableCell>
                            <TableRow>
                                <TableCell align="right">Id</TableCell>
                                <TableCell align="right">First Name</TableCell>
                                <TableCell align="right">Last Name</TableCell>
                                <TableCell align="right">Birth Date</TableCell>
                                <TableCell align="right">Gender</TableCell>
                                <TableCell align="right">Address</TableCell>
                                <TableCell align="right">Medical Records</TableCell>
                                <TableCell align="right">Recomandation</TableCell>

                            </TableRow>
                            {this.state.patients.map(row => {
                            return (
                                <TableRow align="right" key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="right">{row.firstName}</TableCell>
                                    <TableCell align="right">{row.lastName}</TableCell>
                                    <TableCell align="right">{row.birthDate}</TableCell>
                                    <TableCell align="right">{row.gender}</TableCell>
                                    <TableCell align="right">{row.address}</TableCell>
                                    <TableCell align="right">{row.medicalRecord}</TableCell>

                                    <TableCell align="right">{ window.localStorage.getItem("recomandationFromDoctor")}</TableCell>


                                </TableRow>
                            )
                        })}

                        </TableCell>

                    </TableBody>
                </Table>

            </div>
        );
    }
}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default CaregiverViewComponent;