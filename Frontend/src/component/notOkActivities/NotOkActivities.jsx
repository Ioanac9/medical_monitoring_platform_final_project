import React, { Component } from 'react'
import ApiPrescriptionStatus from "../../service/ApiPrescriptionStatus";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import Chart from "../charts/Chart";
import ApiServiceActivities from "../../service/ApiServiceActivities";

class NotOkActivities extends Component {


    constructor(props) {
        super(props)
        this.state = {
            notOkActivities: [],
            observations:' ',
            message: null
        }

        this.reloadNotOkActivitiesList = this.reloadNotOkActivitiesList.bind(this);
    }

    componentDidMount() {
        this.reloadNotOkActivitiesList();
    }

    reloadNotOkActivitiesList() {
        ApiServiceActivities.fetchNotOkActivities()
            .then((res) => {
                this.setState({notOkActivities: res.data.result})
            });
    }


    render() {
        return (
            <div className="App">

                <Typography variant="h4" >Anormal activities</Typography>


                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Activity Id</TableCell>
                            <TableCell align="right">Start Time</TableCell>
                            <TableCell align="right">End Time</TableCell>
                            <TableCell align="right">Label</TableCell>
                            <TableCell align="right">Observations</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.notOkActivities.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.startTime}</TableCell>
                                <TableCell align="right">{row.endTime}</TableCell>
                                <TableCell align="right">{row.label}</TableCell>
                                <TableCell align="right">{row.observations}</TableCell>

                            </TableRow>
                        ))}
                    </TableBody>
                </Table>

            </div>
        );
    }

}


const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default NotOkActivities;