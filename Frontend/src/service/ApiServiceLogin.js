import axios from 'axios';

const USERS_API_BASE_URL = 'http://localhost:8080/users';

class ApiServiceLogin {


    searchUserByPatientId(idPatient){
        return axios.get(USERS_API_BASE_URL + '/' +idPatient);
    }
    deleteUser(idUser){
        return axios.delete(USERS_API_BASE_URL + '/' +idUser);
    }

    fetchUsers(){
        return axios.get(USERS_API_BASE_URL);
    }
    fetchCaregiverByUsername(username,password){
        return axios.get(USERS_API_BASE_URL + '/' +username+'/'+password);
    }

    addUser(user){
        return axios.post(""+USERS_API_BASE_URL, user);
    }

}

export default new ApiServiceLogin();