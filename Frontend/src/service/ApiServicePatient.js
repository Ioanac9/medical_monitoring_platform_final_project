import axios from 'axios';

const PATIENT_API_BASE_URL = 'http://localhost:8080/patient';

class ApiService {

    fetchPatients() {
        return axios.get(PATIENT_API_BASE_URL);
    }

    fetchPatientById(patientId) {
        return axios.get(PATIENT_API_BASE_URL + '/' + patientId);
    }

    deletePatient(patientId) {
        return axios.delete(PATIENT_API_BASE_URL + '/' + patientId);
    }

    addPatient(patient) {
        return axios.post(""+PATIENT_API_BASE_URL, patient);
    }

    editPatient(patient) {
        return axios.put(PATIENT_API_BASE_URL + '/' + patient.id, patient);
    }

}

export default new ApiService();