import axios from 'axios';

const ACTIVITIES_API_BASE_URL = 'http://localhost:8082/restCountActivitati';
const NOT_OK_ACTIVITIES_ABI_BASE_URL = "http://localhost:8082/notOkActivities";

class ApiServiceActivities {

    fetchActivities() {
        return axios.get(ACTIVITIES_API_BASE_URL);
    }
    fetchNotOkActivities() {
        return axios.get(NOT_OK_ACTIVITIES_ABI_BASE_URL);
    }

}

export default new ApiServiceActivities();