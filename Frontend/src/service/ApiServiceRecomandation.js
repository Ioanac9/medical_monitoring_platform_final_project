import axios from 'axios';

const RECOMANDATION_API_BASE_URL = 'http://localhost:8080/recomandation';

class ApiServiceRecomandation {

    fetchRecomandations() {
        return axios.get(RECOMANDATION_API_BASE_URL);
    }

    fetchRecomandationById(recomandationId) {
        return axios.get(RECOMANDATION_API_BASE_URL + '/' + recomandationId);
    }

    deleteRecomandation(recomandationId) {
        return axios.delete(RECOMANDATION_API_BASE_URL + '/' + recomandationId);
    }

    addRecomandation(recomandation) {
        return axios.post(""+RECOMANDATION_API_BASE_URL,recomandation);
    }

    editRecomandation(recomandation) {
        return axios.put(RECOMANDATION_API_BASE_URL + '/' + recomandation.id, recomandation);
    }

}

export default new ApiServiceRecomandation();