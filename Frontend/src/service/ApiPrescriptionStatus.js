import axios from 'axios';

const STATUS_API_BASE_URL = 'http://localhost:8082/medicationPlans';

class ApiPrescriptionStatusActivities {

    fetchStatus() {
        return axios.get(STATUS_API_BASE_URL);
    }

}

export default new ApiPrescriptionStatusActivities();