package com.devglan.controller;

import com.devglan.model.ApiResponse;
import com.devglan.enitites.Patient;
import com.devglan.model.PatientDto;
import com.devglan.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private PatientService patientService;


    @GetMapping
    public ApiResponse<List<Patient>> listPatient(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient list fetched successfully.",patientService.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponse<Patient> getOne(@PathVariable int id){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient fetched successfully.",patientService.findById(id));
    }



}
