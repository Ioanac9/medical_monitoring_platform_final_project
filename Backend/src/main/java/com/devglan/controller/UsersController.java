package com.devglan.controller;

import com.devglan.model.ApiResponse;
import com.devglan.enitites.Users;
import com.devglan.model.UsersDto;
import com.devglan.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;
    @PostMapping
    public ApiResponse<Users> saveUser(@RequestBody UsersDto user){
        return new ApiResponse<>(HttpStatus.OK.value(), "Patient saved successfully.",usersService.save(user));
    }

    @GetMapping
    public ApiResponse<List<Users>> listUsers(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Users list fetched successfully.",usersService.findAll());
    }

    @GetMapping("/{idPatient}")
    public ApiResponse<List<Users>> getOneByPatientIdU(@PathVariable String idPatient  ){
        return new ApiResponse<>(HttpStatus.OK.value(), "User list fetched successfully.",usersService.findByIdPatient(idPatient));
    }

    @GetMapping("/{username}/{password}")
    public ApiResponse<Users> getOneByUsername(@PathVariable String username ,@PathVariable String password){
        return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.",usersService.findByUsername(username,password));
    }

    @PutMapping("/{id}")
    public ApiResponse<UsersDto> update(@RequestBody UsersDto usersDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Users updated successfully.",usersService.update(usersDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        usersService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Users deleted successfully.", null);
    }



}
