package com.devglan.controller;

import com.devglan.model.ApiResponse;
import com.devglan.enitites.Medication;
import com.devglan.model.MedicationDto;
import com.devglan.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/medication")
public class MedicationController {

    @Autowired
    private MedicationService medicationService;

    @PostMapping
    public ApiResponse<Medication> saveMedication(@RequestBody MedicationDto medication){
        return new ApiResponse<>(HttpStatus.OK.value(), "Medication saved successfully.",medicationService.save(medication));
    }

    @GetMapping
    public ApiResponse<List<Medication>> listMedication(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Medication list fetched successfully.",medicationService.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponse<Medication> getOne(@PathVariable int id){
        return new ApiResponse<>(HttpStatus.OK.value(), "Medication fetched successfully.",medicationService.findById(id));
    }

    @PutMapping("/{id}")
    public ApiResponse<MedicationDto> update(@RequestBody MedicationDto medicationDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Medication updated successfully.",medicationService.update(medicationDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        medicationService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Medication deleted successfully.", null);
    }



}
