package com.devglan.controller;

//import com.devglan.model.ApiResponse;
import com.devglan.enitites.Caregiver;
import com.devglan.model.ApiResponse;
import com.devglan.model.CaregiverDto;
import com.devglan.service.CaregiverService;
import com.devglan.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/caregiver")
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;
    private PatientService patientService;

    @PostMapping
    public ApiResponse<Caregiver> saveCaregiver(@RequestBody CaregiverDto caregiver){
        return new ApiResponse<>(HttpStatus.OK.value(), "Caregiver saved successfully.",caregiverService.save(caregiver));
    }

    @GetMapping
    public ApiResponse<List<Caregiver>> listCaregiver(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Caregiver list fetched successfully.",caregiverService.findAll());
    }

     @GetMapping("/{id}")
    public ApiResponse<Caregiver> getOne(@PathVariable int id){
        return new ApiResponse<>(HttpStatus.OK.value(), "Caregiver fetched successfully.",caregiverService.findById(id));
    }

//    @GetMapping("/{id}")
//    public ApiResponse<Caregiver> getOneAndPatientList(@PathVariable int id){
//        Caregiver caregiver = new Caregiver();
//        caregiver.setId(id);
//        return new ApiResponse<>(HttpStatus.OK.value(), "Caregiver and patients fetched successfully.",patientService.findByCaregiver(caregiver));
//    }

    @PutMapping("/{id}")
    public ApiResponse<CaregiverDto> update(@RequestBody CaregiverDto caregiverDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Caregiver updated successfully.",caregiverService.update(caregiverDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        caregiverService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Caregiver deleted successfully.", null);
    }



}
