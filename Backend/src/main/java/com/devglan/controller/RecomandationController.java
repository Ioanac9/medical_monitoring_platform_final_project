package com.devglan.controller;

import com.devglan.model.ApiResponse;
import com.devglan.enitites.Recomandation;
import com.devglan.model.RecomandationDto;
import com.devglan.service.RecomandationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/recomandation")
public class RecomandationController {

    @Autowired
    private RecomandationService recomandationService;
    @PostMapping
    public ApiResponse<Recomandation> saveRecomandation(@RequestBody RecomandationDto recomandation){
        return new ApiResponse<>(HttpStatus.OK.value(), "Recomandation saved successfully.",recomandationService.save(recomandation));
    }

    @GetMapping
    public ApiResponse<List<Recomandation>> listRecomandation(){
        return new ApiResponse<>(HttpStatus.OK.value(), "Recomandation list fetched successfully.",recomandationService.findAll());
    }

//    @GetMapping("/{id}")
//    public ApiResponse<Recomandation> findByPatientId(@PathVariable int id){
//        return new ApiResponse<>(HttpStatus.OK.value(), "Recomandation fetched successfully.",recomandationService.findByPatientId(id));
//    }

    @PutMapping("/{id}")
    public ApiResponse<RecomandationDto> update(@RequestBody RecomandationDto recomandationDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "Recomandation updated successfully.",recomandationService.update(recomandationDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable int id) {
        recomandationService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "Recomandation deleted successfully.", null);
    }



}
