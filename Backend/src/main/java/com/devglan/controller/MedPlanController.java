//package com.devglan.controller;
//
//import com.devglan.model.ApiResponse;
//import com.devglan.enitites.MedPlan;
//import com.devglan.model.MedPlanDto;
//import com.devglan.service.MedPlanService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@CrossOrigin(origins = "*", maxAge = 3600)
//@RestController
//@RequestMapping("/medPlan")
//public class MedPlanController {
//
//    @Autowired
//    private MedPlanService medPlanService;
//
//    @PostMapping
//    public ApiResponse<MedPlan> saveMedPlan(@RequestBody MedPlanDto medPlan){
//        return new ApiResponse<>(HttpStatus.OK.value(), "MedPlan saved successfully.",medPlanService.save(medPlan));
//    }
//
//    @GetMapping
//    public ApiResponse<List<MedPlan>> listMedPlan(){
//        return new ApiResponse<>(HttpStatus.OK.value(), "MedPlan list fetched successfully.",medPlanService.findAll());
//    }
//
//    @GetMapping("/{id}")
//    public ApiResponse<MedPlan> getOne(@PathVariable int id){
//        return new ApiResponse<>(HttpStatus.OK.value(), "MedPlan fetched successfully.",medPlanService.findById(id));
//    }
//
//    @PutMapping("/{id}")
//    public ApiResponse<MedPlanDto> update(@RequestBody MedPlanDto medPlanDto) {
//        return new ApiResponse<>(HttpStatus.OK.value(), "MedPlan updated successfully.",medPlanService.update(medPlanDto));
//    }
//
//    @DeleteMapping("/{id}")
//    public ApiResponse<Void> delete(@PathVariable int id) {
//        medPlanService.delete(id);
//        return new ApiResponse<>(HttpStatus.OK.value(), "MedPlan deleted successfully.", null);
//    }
//
//
//
//}
