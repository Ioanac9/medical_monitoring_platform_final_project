package com.devglan.enitites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "prescription")
public class Prescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @ManyToOne
//    @JoinColumn(name="patientId")
//    @JsonIgnore
//
//    private Patient patientId;



    @Column
    private Integer intokeInterval;

    @Column
    private Integer periodTreatment;

    @Column
    private String tokenPill;

//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "medication_id", nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore

//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "medication_id")
//    private Medication medication_id;

//    @OneToOne(fetch = FetchType.LAZY,
//            cascade =  CascadeType.ALL,
//            mappedBy = "prescription")
//    private Medication medication_id;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "medication", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)

    private Medication medication;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Integer getIntokeInterval() {
        return intokeInterval;
    }

    public void setIntokeInterval(Integer intokeInterval) {
        this.intokeInterval = intokeInterval;
    }

    public Integer getPeriodTreatment() {
        return periodTreatment;
    }

    public void setPeriodTreatment(Integer periodTreatment) {
        this.periodTreatment = periodTreatment;
    }

    public String getTokenPill() {
        return tokenPill;
    }

    public void setTokenPill(String tokenPill) {
        this.tokenPill = tokenPill;
    }

    //
//    public MedPlan getMedPlan() {
//        return medPlan;
//    }


//    public Patient getPatientId() {
//        return patientId;
//    }
//
//    public void setPatient(Patient patient) {
//        this.patientId = patient;
//    }

//    public void setMedication(Medication medication) {
//        this.medication_id = medication;
//    }
//
//    public Medication getMedication() {
//        return medication_id;
//    }


//    public Medication getMedication() {
//        return this.medication;
//    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }
}
