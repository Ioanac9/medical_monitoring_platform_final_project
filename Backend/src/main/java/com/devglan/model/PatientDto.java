package com.devglan.model;

import com.devglan.enitites.*;
//import com.devglan.enitites.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class PatientDto {

    private int id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private Set<Prescription> prescriptions;
    private Recomandation recomandation;

    private Caregiver Caregiver_idCaregiver;
    private int caregiverId;
    private String caregiverFirstName;
    private String caregiverLastName;
    private LocalDate caregiverBirthDate;
    private String caregiverGender;
    private String caregiverAddress;
    private Set<Patient> caregiverPatients;

    private Users user;
    private String userUsername;

    public String getUserUsername() {
        return userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }
    //   private User user;
  /***************************************************************************************************/
    public Caregiver getCaregiver() {
        return Caregiver_idCaregiver;
    }

    public int getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(int caregiverId) {
        this.caregiverId = caregiverId;
    }

    public String getCaregiverFirstName() {
        return caregiverFirstName;
    }

    public void setCaregiverFirstName(String caregiverFirstName) {
        this.caregiverFirstName = caregiverFirstName;
    }

    public String getCaregiverLastName() {
        return caregiverLastName;
    }

    public void setCaregiverLastName(String caregiverLastName) {
        this.caregiverLastName = caregiverLastName;
    }

    public LocalDate getCaregiverBirthDate() {
        return caregiverBirthDate;
    }

    public void setCaregiverBirthDate(LocalDate caregiverBirthDate) {
        this.caregiverBirthDate = caregiverBirthDate;
    }

    public String getCaregiverGender() {
        return caregiverGender;
    }

    public void setCaregiverGender(String caregiverGender) {
        this.caregiverGender = caregiverGender;
    }

    public String getCaregiverAddress() {
        return caregiverAddress;
    }

    public void setCaregiverAddress(String caregiverAddress) {
        this.caregiverAddress = caregiverAddress;
    }

    public Set<Patient> getCaregiverPatients() {
        return caregiverPatients;
    }

    public void setCaregiverPatients(Set<Patient> caregiverPatients) {
        this.caregiverPatients = caregiverPatients;
    }
    //    public void setCaregiver(Caregiver caregiver) {
//        this.caregiver = caregiver;
//    }

    public Recomandation getRecomandation() {
        return recomandation;
    }

    public void setRecomandation(Recomandation recomandation) {
        this.recomandation = recomandation;
    }

    /*******************************************************************************************/



    public LocalDate getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /*******************************************************************************/
    public Set<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Set<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
