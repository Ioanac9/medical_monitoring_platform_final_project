//package com.devglan.service.impl;
//
//import com.devglan.dao.MedPlanDao;
//import com.devglan.enitites.MedPlan;
//import com.devglan.enitites.Patient;
//import com.devglan.model.MedPlanDto;
//import com.devglan.service.MedPlanService;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//
//@Service(value = "medPlanService")
//public class MedPlanServiceImpl implements MedPlanService {
//
//    @Autowired
//    private MedPlanDao medPlanDao;
//
//    public List<MedPlan> findAll() {
//
//       return  medPlanDao.findAll();
//
//    }
//
//    @Override
//    public void delete(int id) {
//        medPlanDao.deleteById(id);
//    }
//
//
//    @Override
//    public MedPlan findById(int id) {
//        Optional<MedPlan> optionalMedPlan = medPlanDao.findById(id);
//        return optionalMedPlan.isPresent() ? optionalMedPlan.get() : null;
//    }
//
//    @Override
//    public MedPlanDto update(MedPlanDto medPlanDto) {
//        MedPlan medPlan = findById(medPlanDto.getId());
//        if (medPlan != null) {
//            BeanUtils.copyProperties(medPlanDto, medPlan, "password", "medPlanname");
//            medPlanDao.save(medPlan);
//        }
//        return medPlanDto;
//    }
//
//    @Override
//    public MedPlan save(MedPlanDto medPlan) {
//        MedPlan newMedPlan = new MedPlan();
//        Patient currentPatient = new Patient();
//        currentPatient.setId(medPlan.getPatientId());
//        newMedPlan.setPatient(currentPatient);
//        newMedPlan.setPrescriptions(medPlan.getPrescriptions());
//        return medPlanDao.save(newMedPlan);
//    }
//}
