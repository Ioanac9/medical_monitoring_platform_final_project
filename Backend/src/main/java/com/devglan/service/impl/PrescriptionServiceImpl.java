package com.devglan.service.impl;

import com.devglan.dao.CaregiverDao;
import com.devglan.dao.PrescriptionDao;
import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Medication;
import com.devglan.enitites.Patient;
import com.devglan.enitites.Prescription;
import com.devglan.model.PrescriptionDto;
import com.devglan.service.PrescriptionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "prescriptionService")
public class PrescriptionServiceImpl implements PrescriptionService {

    @Autowired
    private PrescriptionDao prescriptionDao;
    private CaregiverDao caregiverDao;

    public List<Prescription> findAll() {
        List<Prescription> list = new ArrayList<>();
        //prescriptionDao.findAll().iterator().forEachRemaining(list::add);
        return prescriptionDao.findAll();
    }

    @Override
    public void delete(int id) {
        prescriptionDao.deleteById(id);
    }

//    @Override
//    public List<Prescription> findByPatientId(int patientId){
//        return prescriptionDao.findByPatientId(patientId);
//    }

//@PersistenceContext
//private EntityManager entityManager;
//
//    @SuppressWarnings("unchecked")
//    @Override
//    public  List<Prescription> findByCaregiver_idCaregiver(Caregiver caregiver) {
//
//        return this.entityManager.
//                createQuery("select e from prescription e where e.prescription_id like '"+caregiver.getId()+"'").
//                getResultList();
//        List<Prescription> list = new ArrayList<>();
//
//        prescriptionDao.findByCaregiver(caregiver).iterator().forEachRemaining(list::add);
//
//        Optional<Caregiver> optionalCaregiver = caregiverDao.findById(caregiver.getId());
//        caregiver = optionalCaregiver.isPresent() ? optionalCaregiver.get() : null;
//        if (caregiver != null) {
//            return (List<Prescription>) prescriptionDao.findByCaregiver(caregiver);
//        }
//        return null;

    //  }


    @Override
    public Prescription findById(int id) {
        Optional<Prescription> optionalPrescription = prescriptionDao.findById(id);
        return optionalPrescription.isPresent() ? optionalPrescription.get() : null;
    }

    @Override
    public PrescriptionDto update(PrescriptionDto prescriptionDto) {
        Prescription prescription = findById(prescriptionDto.getId());
        if (prescription != null) {
            BeanUtils.copyProperties(prescriptionDto, prescription,  "intokeInterval","periodTreatment");
            prescriptionDao.save(prescription);
        }
        return prescriptionDto;
    }

    @Override
    public Prescription save(PrescriptionDto prescription) {
        Prescription newPrescription = new Prescription();
        newPrescription.setIntokeInterval(prescription.getIntokeInterval());
        newPrescription.setPeriodTreatment(prescription.getPeriodTreatment());

//        Patient localPatient = new Patient();
//        localPatient.setId(prescription.getPatientId());
//
//        newPrescription.setPatient(localPatient);

        Medication localMedication = new Medication();
        localMedication.setId(prescription.getMedicationId());

        newPrescription.setMedication(localMedication);

        return prescriptionDao.save(newPrescription);
    }
}
