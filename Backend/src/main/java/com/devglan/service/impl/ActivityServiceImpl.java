package com.devglan.service.impl;

import com.devglan.dao.ActivityDao;
import com.devglan.enitites.Activity;
import com.devglan.enitites.Patient;
import com.devglan.model.ActivityDto;
import com.devglan.service.ActivityService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "activityService")
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityDao activityDao;

    public List<Activity> findAll() {
        // List<Activity> list = new ArrayList<>();
        return activityDao.findAll();

    }

    @Override
    public void delete(int id) {
        activityDao.deleteById(id);
    }

    @Override
    public Activity findById(int id) {
        Optional<Activity> optionalActivity = activityDao.findById(id);
        return optionalActivity.isPresent() ? optionalActivity.get() : null;
    }

    @Override
    public ActivityDto update(ActivityDto activityDto) {
        Activity activity = findById(activityDto.getId());
        if (activity != null) {
            Patient localPatient = new Patient();
            localPatient.setId(activityDto.getPatientId().getId());
            if(localPatient.getId() != 0){
                activity.setPatientId(localPatient);}
            BeanUtils.copyProperties(activityDto, activity, "password", "activityname","prescription","patient");
            activityDao.save(activity);
        }else{
            BeanUtils.copyProperties(activityDto, activity, "password", "activityname","prescription","patient");
        }
        return activityDto;
    }

    @Override
    public Activity save(ActivityDto activity) {
        Activity newActivity = new Activity();
        newActivity.setStart_time(activity.getStart_time());
        newActivity.setEnd_time(activity.getEnd_time());
        newActivity.setLabel(activity.getLabel());
        newActivity.setPatientId(activity.getPatientId());
        return activityDao.save(newActivity);
    }
}
