package com.devglan.service.impl;

import com.devglan.dao.CaregiverDao;
import com.devglan.dao.UsersDao;
import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Users;
import com.devglan.model.CaregiverDto;
import com.devglan.service.CaregiverService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "caregiverService")
public class CaregiverServiceImpl implements CaregiverService {

    @Autowired
    private UsersDao usersDao;

    @Autowired
    private CaregiverDao caregiverDao;


    public List<Caregiver> findAll() {
        List<Caregiver> list = new ArrayList<>();
       return  caregiverDao.findAll();

    }

    @Override
    public void delete(int id) {
        caregiverDao.deleteById(id);
    }

    @Override
    public Caregiver findOne(String firstName) {
        return caregiverDao.findByFirstName(firstName);
    }



    @Override
    public Caregiver findById(int id) {
        Optional<Caregiver> optionalCaregiver = caregiverDao.findById(id);
        return optionalCaregiver.isPresent() ? optionalCaregiver.get() : null;
    }

    @Override
    public CaregiverDto update(CaregiverDto caregiverDto) {
        Caregiver caregiver = findById(caregiverDto.getId());
        if (caregiver != null) {
            BeanUtils.copyProperties(caregiverDto, caregiver,  "caregivername","patients","user");
            caregiverDao.save(caregiver);
        }
        return caregiverDto;
    }

    @Override
    public Caregiver save(CaregiverDto caregiver) {
        Caregiver newCaregiver = new Caregiver();
        newCaregiver.setFirstName(caregiver.getFirstName());
        newCaregiver.setLastName(caregiver.getLastName());
        newCaregiver.setBirthDate(caregiver.getBirthDate());
        newCaregiver.setGender(caregiver.getGender());
        newCaregiver.setAddress(caregiver.getAddress());

        Users userNou = new Users();
       // userNou.setUsername(caregiver.getUser().getUsername());
        userNou =  usersDao.findByUsername(caregiver.getUser().getUsername());
//        Users userLocal = new Users();
//        userLocal.setId(caregiver.getUser().getId());
        newCaregiver.setUser(userNou);
       // newCaregiver.setPatientsList(caregiver.getPatients());

        return caregiverDao.save(newCaregiver);
    }
}
