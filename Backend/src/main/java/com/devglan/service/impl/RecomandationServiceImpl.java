package com.devglan.service.impl;

import com.devglan.dao.RecomandationDao;
import com.devglan.enitites.Recomandation;
import com.devglan.enitites.Patient;
import com.devglan.model.RecomandationDto;
import com.devglan.service.RecomandationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service(value = "recomandationService")
public class RecomandationServiceImpl implements RecomandationService {

    @Autowired
    private RecomandationDao recomandationDao;

    public List<Recomandation> findAll() {
        // List<Recomandation> list = new ArrayList<>();
        return recomandationDao.findAll();

    }

    @Override
    public void delete(int id) {
        recomandationDao.deleteById(id);
    }

    @Override
    public Recomandation findById(int id) {
        Optional<Recomandation> optionalRecomandation = recomandationDao.findById(id);
        return optionalRecomandation.isPresent() ? optionalRecomandation.get() : null;
    }

    @Override
    public RecomandationDto update(RecomandationDto recomandationDto) {
        Recomandation recomandation = findById(recomandationDto.getId());
        if (recomandation != null) {
            Patient localPatient = new Patient();
            localPatient.setId(recomandationDto.getPatientId().getId());
            if(localPatient.getId() != 0){
                recomandation.setPatientId(localPatient);}
            BeanUtils.copyProperties(recomandationDto, recomandation, "password", "recomandationname","prescription","patient");
            recomandationDao.save(recomandation);
        }else{
            BeanUtils.copyProperties(recomandationDto, recomandation, "password", "recomandationname","prescription","patient");
        }
        return recomandationDto;
    }

    @Override
    public Recomandation save(RecomandationDto recomandation) {
        Recomandation newRecomandation = new Recomandation();
        newRecomandation.setRecomandation(recomandation.getRecomandation());
        newRecomandation.setPatientId(recomandation.getPatientId());
        return recomandationDao.save(newRecomandation);
    }
}
