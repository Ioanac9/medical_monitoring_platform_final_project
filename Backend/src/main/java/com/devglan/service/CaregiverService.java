package com.devglan.service;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Patient;
import com.devglan.model.CaregiverDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CaregiverService {

    Caregiver save(CaregiverDto caregiver);
    List<Caregiver> findAll();
    void delete(int id);

    Caregiver findOne(String firstname);

    Caregiver findById(int id);


    CaregiverDto update(CaregiverDto caregiverDto);
}
