package com.devglan.service;

import com.devglan.enitites.Recomandation;
import com.devglan.model.RecomandationDto;

import java.util.List;

public interface RecomandationService {

    Recomandation save(RecomandationDto recomandation);
    List<Recomandation> findAll();
    void delete(int id);

    Recomandation findById(int id);

    RecomandationDto update(RecomandationDto recomandationDto);
}
