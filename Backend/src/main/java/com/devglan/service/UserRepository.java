package com.devglan.service;

import com.devglan.enitites.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer>{

    User findByUsername(String username);

}
