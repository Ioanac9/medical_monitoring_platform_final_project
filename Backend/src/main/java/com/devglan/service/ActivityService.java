package com.devglan.service;

import com.devglan.enitites.Activity;
import com.devglan.model.ActivityDto;

import java.util.List;

public interface ActivityService {

    Activity save(ActivityDto medication);
    List<Activity> findAll();
    void delete(int id);

    Activity findById(int id);

    ActivityDto update(ActivityDto medicationDto);
}
