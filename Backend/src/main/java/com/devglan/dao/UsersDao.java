package com.devglan.dao;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersDao extends JpaRepository<Users, Integer> {

    public Users findByUsername(String username);
    public  Users findByIdPatient(String idPatient);



}
