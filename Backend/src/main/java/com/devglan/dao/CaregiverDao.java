package com.devglan.dao;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaregiverDao extends JpaRepository<Caregiver, Integer> {

    Caregiver findByFirstName(String firstName);

}
