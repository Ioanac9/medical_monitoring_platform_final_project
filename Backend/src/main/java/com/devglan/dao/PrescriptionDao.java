package com.devglan.dao;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrescriptionDao extends JpaRepository<Prescription, Integer> {


//    List<Prescription> findByPatientId(int patientId);
}
