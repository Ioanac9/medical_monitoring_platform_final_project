package com.devglan.dao;

import com.devglan.enitites.Activity;
import com.devglan.enitites.Recomandation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecomandationDao extends JpaRepository<Recomandation, Integer> {

}