package com.devglan.dao;

import com.devglan.enitites.Caregiver;
import com.devglan.enitites.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientDao extends JpaRepository<Patient, Integer> {

    Patient findByFirstName(String firstName);
   // Patient findByCaregiver_idCaregiver(Caregiver caregiver_idCaregiver);
  // List<Patient> findAllByUser(String id);
}
