package com.devglan.dao;

import com.devglan.enitites.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationDao extends JpaRepository<Medication, Integer> {

    Medication findByName(String name);
}
