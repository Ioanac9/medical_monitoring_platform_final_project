
package com.example.consumingwebservice;

import com.example.consumingwebservice.clientModel.ActivityClient;
import com.example.consumingwebservice.clientModel.ActivityCount;
import com.example.consumingwebservice.clientModel.MedicationClient;

import com.example.consumingwebservice.wsdl.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ConsumingWebServiceApplication {
	public List<Activity> activities;
	public List<Activity> notOkActivities = new ArrayList<Activity>();
	public List<Medication> medications;
	public String label;
	public int count;
	public List <ActivityCount>activitiesCount;

	public static void main(String[] args) {
		SpringApplication.run(ConsumingWebServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner lookup(CountryClient quoteClient) {
		return args -> {
			String country = "Spain";

			if (args.length > 0) {
				country = args[0];
			}
			GetCountryResponse response = quoteClient.getCountry(country);
			System.out.println(response.getCountry().getCapital());
			System.err.println(response.getCountry().getCurrency());
		};
	}

    @Bean
    CommandLineRunner lookup2(ActivityClient quoteClient) {
        return args -> {
            String idPatient = "1";

            if (args.length > 0) {
                idPatient = args[0];
            }
            GetActivityResponse response = quoteClient.getActivity(idPatient);

			List<Activity> activity;
			activity= response.getActivityList().getActivity();
			activities = activity;

			int countShower =0;
			int countBreakFast =0;
			int countToileting =0;
			int countSleeping =0;
			int countGrooming =0;
			int countSpare_Time =0;
			int countLunch =0;
			int countLeaving=0;

			for(Activity al: activity) {
				System.out.println("Id: "+al.getId() +", start time: "+ al.getStartTime()
				+", end time: "+ al.getEndTime()+", label: "+al.getLabel()+", id patient: "
				+al.getPatientId());

				if(al.getLabel().equals("Showering")){
						countShower++;
					}
				if(al.getLabel().equals("Toileting")){
					countToileting++;
				}
				if(al.getLabel().equals("Grooming")){
					countGrooming++;
				}
				if(al.getLabel().equals("Spare_Time/TV")){
					countSpare_Time++;
				}
				if(al.getLabel().equals("Breakfast")){
					countBreakFast++;
				}
				if(al.getLabel().equals("Sleeping")){
					countSleeping++;
				}
				if(al.getLabel().equals("Lunch")){
					countLunch++;
				}
				if(al.getLabel().equals("Leaving")){
					countLeaving++;
				}
				if(!al.getObservations().equals("Normal activity")){
					Activity ac = new Activity();
					ac.setId(al.getId());
					ac.setStartTime(al.getStartTime());
					ac.setEndTime(al.getEndTime());
					ac.setLabel(al.getLabel());
					ac.setObservations(al.getObservations());

					notOkActivities.add(ac);
				}


			}
			List<ActivityCount> activityCountList = new  ArrayList<ActivityCount>();
			ActivityCount ac = new ActivityCount();
			ac.setLabel("Showering");
			ac.setCount(countShower);
			activityCountList.add(ac);
			ActivityCount ac1 = new ActivityCount();
			ac1.setLabel("Toileting");
			ac1.setCount(countToileting);
			activityCountList.add(ac1);
			ActivityCount ac2 = new ActivityCount();
			ac2.setLabel("Grooming");
			ac2.setCount(countGrooming);
			activityCountList.add(ac2);
			ActivityCount ac3 = new ActivityCount();
			ac3.setLabel("Spare_Time/TV");
			ac3.setCount(countSpare_Time);
			activityCountList.add(ac3);
			ActivityCount ac4 = new ActivityCount();
			ac4.setLabel("Breakfast");
			ac4.setCount(countBreakFast);
			activityCountList.add(ac4);
			ActivityCount ac5 = new ActivityCount();
			ac5.setLabel("Sleeping");
			ac5.setCount(countSleeping);
			activityCountList.add(ac5);
			ActivityCount ac6 = new ActivityCount();
			ac6.setLabel("Lunch");
			ac6.setCount(countLunch);
			activityCountList.add(ac6);
			ActivityCount ac7 = new ActivityCount();
			ac7.setLabel("Leaving");
			ac7.setCount(countLeaving);
			activityCountList.add(ac7);
			activitiesCount = activityCountList;

        };
    }

	@Bean
	CommandLineRunner lookup3(MedicationClient quoteClient) {

		return args -> {
			String patientId = "1";

			if (args.length > 0) {
				patientId = args[0];
			}
			GetMedicationResponse response = quoteClient.getMedication(patientId);
			List<Medication> medication;
			medication = response.getMedicationList().getMedication();
			medications = medication;
			for (Medication al : medications) {
				System.out.println("Medication id: " + al.getMedicationId() + ", name: " + al.getName()
						+ ", dosage: " + al.getDosage() + ", sideEffects: " + al.getSideEffects()
						+ ", id patient: " + al.getPatientId() + ", id prescription: "
						+ al.getIdPrescription() + ", intoke Interval: " + al.getIntokeInterval()
						+ ", period: " + al.getPeriodTreatment() + ", token pill: "
						+ al.getTokenPill());
			}


		};
	}


}
