
package com.example.consumingwebservice.clientModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.example.consumingwebservice.wsdl.GetActivityRequest;
import com.example.consumingwebservice.wsdl.GetActivityResponse;


public class ActivityClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(ActivityClient.class);

    public GetActivityResponse getActivity(String idPatient) {

        GetActivityRequest request = new GetActivityRequest();
        request.setPatientId(idPatient);

        log.info("Requesting location for " + idPatient);

        GetActivityResponse response = (GetActivityResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8092/ws/countries", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetActivityRequest"));

        return response;
    }

}
