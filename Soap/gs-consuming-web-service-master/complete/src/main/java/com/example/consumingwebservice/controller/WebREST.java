package com.example.consumingwebservice.controller;

import com.example.consumingwebservice.clientModel.ActivityCount;
import com.example.consumingwebservice.ConsumingWebServiceApplication;
import com.example.consumingwebservice.wsdl.Activity;
import com.example.consumingwebservice.wsdl.Medication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebREST {

    private static Logger log = LoggerFactory.getLogger(WebREST.class);
    @Autowired
    ConsumingWebServiceApplication activititati;

    @Autowired
    ActivityCount activityCount;

    @RequestMapping("/restActivitati")
    public ApiResponse<Activity> getMessage()
    {
       return new ApiResponse<>(HttpStatus.OK.value(), "Activity list fetched successfully.",activititati.activities);
    }

    @RequestMapping("/restCountActivitati")
    public ApiResponse<ActivityCount> getActivity(){
        return new ApiResponse<>(HttpStatus.OK.value(),"Activity count fetched successfully.",activititati.activitiesCount);

    }

    @RequestMapping("/medicationPlans")
    public ApiResponse<Medication> getMedicationPlans(){
        return new ApiResponse<>(HttpStatus.OK.value(),"Medication plans fetched successfully.",activititati.medications);

    }

    @RequestMapping("/notOkActivities")
    public ApiResponse<Activity> getNotOkActivities(){
        return new ApiResponse<>(HttpStatus.OK.value(),"Not ok activity list fetched successfully.",activititati.notOkActivities);

    }


}