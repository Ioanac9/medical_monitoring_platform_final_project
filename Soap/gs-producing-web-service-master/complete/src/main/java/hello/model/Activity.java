package hello.model;

public class Activity {

    private int id;
    private String start_time;
    private String end_time;
    private String label;
    private String observations;
    private int patientId;

    public String getId() {
        return String.valueOf(id);
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPatientId() {
        return String.valueOf(patientId);
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }
}