package hello.model;

public class Prescription {
    private int id;
    private Integer intokeInterval;
    private Integer periodTreatment;
    private String tokenPill;
    private int medicationId;
    private int patientId;

    public String getTokenPill() {
        return tokenPill;
    }

    public void setTokenPill(String tokenPill) {
        this.tokenPill = tokenPill;
    }

    public int getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(int medicationId) {
        this.medicationId = medicationId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIntokeInterval() {
        return intokeInterval;
    }

    public void setIntokeInterval(Integer intokeInterval) {
        this.intokeInterval = intokeInterval;
    }

    public Integer getPeriodTreatment() {
        return periodTreatment;
    }

    public void setPeriodTreatment(Integer periodTreatment) {
        this.periodTreatment = periodTreatment;
    }

}
