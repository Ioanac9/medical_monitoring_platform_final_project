package hello.dao;

import hello.model.Prescription;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class PrescriptionDB {

    Statement statement = null;
    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement findStatement = null;


    private static final Logger LOGGER = Logger.getLogger(PrescriptionDB.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/test";
    private static final String USER = "root";


    public Prescription printAllPrescriptionsForPatientId(int  medicationId )  {

        String  query = "SELECT * FROM prescription where medication = ?";
        Prescription prescription = new Prescription();

        ResultSet rs = null;

        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            findStatement = connection.prepareStatement(query);
            findStatement.setInt(1, medicationId);
            resultSet = findStatement.executeQuery();

            while (resultSet.next()) {

                Prescription currentPrescription = new Prescription();
                int id = resultSet.getInt("id");
                String intokeInterval = resultSet.getString("intokeInterval");
                String periodTreatment = resultSet.getString("periodTreatment");
                String tokenPill = resultSet.getString("tokenPill");
                int medication = resultSet.getInt("medication");


                currentPrescription.setId(resultSet.getInt("id"));
                currentPrescription.setIntokeInterval(resultSet.getInt("intokeInterval"));
                currentPrescription.setPeriodTreatment(resultSet.getInt("periodTreatment"));
                currentPrescription.setTokenPill(resultSet.getString("tokenPill"));
                currentPrescription.setMedicationId(resultSet.getInt("medication"));


                // print the results
                LOGGER.info( id+ " "+ intokeInterval +" "+ periodTreatment+" "+ tokenPill+" "+medication);
                prescription=currentPrescription;
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }
        return prescription;
    }


}
