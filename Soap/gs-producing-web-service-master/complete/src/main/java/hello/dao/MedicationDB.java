package hello.dao;

import hello.model.Medication;
import hello.model.Prescription;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MedicationDB {

    Statement statement = null;
    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement findStatement = null;


    private static final Logger LOGGER = Logger.getLogger(MedicationDB.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/test";
    private static final String USER = "root";


    public List<Medication> printAllMedicationsForPatientId(int idPatient )  {

        String  query = "SELECT * FROM medication where patientId = ?";
        List<Medication> medicationList = new ArrayList();

        ResultSet rs = null;
        PrescriptionDB pdb = new PrescriptionDB();
        Prescription prescription = new Prescription();


        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            findStatement = connection.prepareStatement(query);
            findStatement.setInt(1, idPatient);
            resultSet = findStatement.executeQuery();

            while (resultSet.next()) {

                Medication currentMedication = new Medication();
                int id = resultSet.getInt("id");
                String dosage = resultSet.getString("dosage");
                String name = resultSet.getString("name");
                String sideEffects = resultSet.getString("sideEffects");

                prescription = pdb.printAllPrescriptionsForPatientId(Integer.valueOf(id));


                currentMedication.setMedicationId(resultSet.getInt("id"));
                currentMedication.setDosage(resultSet.getInt("dosage"));
                currentMedication.setName(resultSet.getString("name"));
                currentMedication.setSideEffects(resultSet.getString("sideEffects"));
                currentMedication.setPatientId(resultSet.getInt("patientId"));
                currentMedication.setIntokeInterval(prescription.getIntokeInterval());
                currentMedication.setPeriodTreatment(prescription.getPeriodTreatment());
                currentMedication.setPrescriptionId(prescription.getId());
                currentMedication.setTokenPill(prescription.getTokenPill());


                // print the results
                LOGGER.info( id+ " "+ dosage +" "+ name+" "+ sideEffects+" ");
                medicationList.add(currentMedication);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }
        return medicationList;
    }


}
