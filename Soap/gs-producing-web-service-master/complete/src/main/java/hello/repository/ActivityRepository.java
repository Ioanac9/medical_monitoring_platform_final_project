package hello.repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import hello.dao.ActivityDB;
import io.spring.guides.gs_producing_web_service.Activity;
import io.spring.guides.gs_producing_web_service.ActivityList;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ActivityRepository {
    private static final List<Activity> activities = new ArrayList<Activity>();

    @PostConstruct

    public void initData() {
        ActivityDB medBD= new ActivityDB();

        List<hello.model.Activity> medicationList = medBD.printAllActivitiesForPatientId(1);

        for(hello.model.Activity it : medicationList) {
            Activity activity = new Activity();
            activity.setPatientId(it.getPatientId());
            activity.setEndTime(it.getEnd_time());
            activity.setStartTime(it.getStart_time());
            activity.setLabel(it.getLabel());
            activity.setObservations(it.getObservations());
            activity.setId(it.getId());
            activities.add(activity);
        }

    }

    public ActivityList findActivity(String patientId) {
        ActivityList al= new ActivityList(activities);
       return al;
    }
}
