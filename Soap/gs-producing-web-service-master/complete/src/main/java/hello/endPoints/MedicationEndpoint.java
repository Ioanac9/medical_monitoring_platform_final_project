package hello.endPoints;

import hello.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import io.spring.guides.gs_producing_web_service.GetMedicationRequest;
import io.spring.guides.gs_producing_web_service.GetMedicationResponse;

@Endpoint
public class MedicationEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private MedicationRepository medicationRepository;

    @Autowired
    public MedicationEndpoint(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMedicationRequest")
    @ResponsePayload
    public GetMedicationResponse getMedication(@RequestPayload GetMedicationRequest request) {
        GetMedicationResponse response = new GetMedicationResponse();
        response.setMedicationList( medicationRepository.findMedication(request.getPatientId()));

        return response;
    }
}
