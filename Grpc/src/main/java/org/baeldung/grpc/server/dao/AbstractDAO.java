package org.baeldung.grpc.server.dao;

import org.baeldung.grpc.server.databaseConnection.Conexiune;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AbstractDAO<T> {

    private final Class<T> type;
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    protected List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (Exception e) {
            e.getCause();
        }
        return list;
    }

    public List<T> selectAll() {
        PreparedStatement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        String query = createSelectAllStatement();
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            e.getCause();
        } finally {
            Conexiune.close(statement);
            Conexiune.close(resultSet);
            Conexiune.close(connection);
        }		return null;
    }
    //se creeaza sintaxa pentru interogare
    private String createSelectQuery(String name) {
        String selectString ;
        selectString="SELECT * FROM "+type.getSimpleName()+" WHERE "+name+" =? ";
        return selectString;
    }
    public String createSelectAllStatement(){
        String selectAllString;
        selectAllString= "SELECT * FROM "+type.getSimpleName();
        return  selectAllString;
    }
    //se creeaza sintaxa de adaugare
    public String createInsertStatement(int n){
        String insertString ;
        insertString=" INSERT INTO "+type.getSimpleName()+" VALUES (";
        for(int i=0;i<n-1;i++)
            insertString=insertString+"? ,";
        insertString=insertString+"?) ";
        return insertString;
    }
    // se executa inserarea in tabela dorita
    public T insert(ArrayList<Object> elements) {
        PreparedStatement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        String query = createInsertStatement(elements.size());//se initiaza query-ul
        try {
            connection = Conexiune.getConnection();//se realizeaza conexiunea la baza de date
            statement = connection.prepareStatement(query);
            int i=1;
            //se inlocuiesc campurile "?" din query cu valorile dorite
            for(Object it : elements){//pentru fiecare obiect care se doreste a se adauga in tabela
                if(it instanceof Integer)// se verifica tipul lui si se insereaza in funtie de asta
                    statement.setInt(i, (int)it);//se adauga parametri query-ului => "?" este inlocuit cu o valoare
                else
                {if(it instanceof String)
                    statement.setString(i, (String)it);
                }
                i++;
            }
            statement.execute();	//se executa query-ul
            return createObjects(resultSet).get(0);//se realizeaza conversia rezultatului intr-un obiect de tip generic si se returneaza prima valoare din resultSet
        } catch (SQLException e) {
            e.getCause();
        } finally {
            Conexiune.close(statement);
            Conexiune.close(resultSet);
            Conexiune.close(connection);
        }
        return null;

    }

    public String createUpdateStatement(ArrayList<String> names,String field){
        String updateString;
        updateString="UPDATE "+type.getSimpleName()+" SET ";
        for(String it : names){
            updateString=updateString+it+" =? ";
            if(it!=names.get(names.size()-1))
                updateString=updateString+",";
        }
        updateString=updateString+" WHERE "+field+" = ?";
        return updateString;
    }

    public T update(ArrayList<Object> list,ArrayList<String> names,int id,String field ) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createUpdateStatement(names,field);

        try {
            int i=1;
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            for(Object it : list){
                if(it instanceof Integer)
                    statement.setInt(i, (int)it);
                else {
                    if(it instanceof String)
                        statement.setString(i, (String)it);
                }
                i++;
            }

            statement.setInt(i, id);
            statement.execute();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            e.getCause();
        } finally {
            Conexiune.close(statement);
            Conexiune.close(resultSet);
            Conexiune.close(connection);
        }
        return null;
    }

    public String createDeleteStatement(ArrayList<String> names ){
        String deleteString;
        deleteString="DELETE FROM "+type.getSimpleName()+" WHERE ";
        for(String it : names){
            deleteString=deleteString+it+" =? ";
            if(it!=names.get(names.size()-1)) // se merge pana la urlimul camp pe care dorim sa il stergem pentru a nu avea ultimul cuvant "and"
                deleteString=deleteString+"and ";
        }
        return deleteString;
    }

    public T delete(ArrayList<Object> list,ArrayList<String> names ) {
        int i=1;
        PreparedStatement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        String query = createDeleteStatement(names);
        System.out.println(query);
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);

            for(Object it : list){
                if(it instanceof Integer)
                    statement.setInt(i, (int)it);
                else {
                    if(it instanceof String)
                        statement.setString(i, (String)it);
                }
                i++;
            }
            statement.execute();
            System.out.println("intra");
            return createObjects(resultSet).get(0);

        } catch (SQLException e) {
            e.getCause();
            System.out.println("intra");
        } finally {
            Conexiune.close(statement);
            Conexiune.close(resultSet);
            Conexiune.close(connection);
        }
        return null;

    }

    public T findById(int id,String name) {
        PreparedStatement statement = null;
        Connection connection = null;
        ResultSet resultSet = null;
        String query = createSelectQuery(name);
        try {
            connection = Conexiune.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            e.getCause();
        } finally {
            Conexiune.close(statement);
            Conexiune.close(resultSet);
            Conexiune.close(connection);
        }
        return null;
    }

}