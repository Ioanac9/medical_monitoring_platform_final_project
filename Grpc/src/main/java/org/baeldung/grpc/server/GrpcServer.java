package org.baeldung.grpc.server;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.baeldung.grpc.server.bussinesLogic.PatientBLL;
import org.baeldung.grpc.server.databaseConnection.Conexiune;
import org.baeldung.grpc.server.model.Patient;

public class GrpcServer {
    private static final Logger LOGGER = Logger.getLogger(GrpcServer.class.getName());

    public static void main(String[] args) throws IOException, InterruptedException {

        ///////////////////////////////////////////////////////////////////
//        int i=1;
//        LocalTime localTime = LocalTime.parse( "23:03:01" );
//        System.out.println("Wait until the time of downloading medication plan,witch is: " +localTime);
//        while(i==1) {
//
//            LocalTime time = LocalTime.now();
//            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
//            //System.out.println("Ora curenta: " + time.format(formatter));
//            ///////////////////////////////////////////////////////////////
//
//            if (localTime.toString().equals(time.format(formatter))) {
//                System.out.println("Ore egale");
//                i=0;

                // se creeaza serverul si ii atribuim serviciul creat inn fisierul .proto
                Server server = ServerBuilder.forPort(8081)
                        .addService(new HelloServiceImpl()).build();

                System.out.println("Starting server...");
                //se porneste serverul
                server.start();
                System.out.println("Server started!");
                server.awaitTermination();
//            }
//
//        }

    }
}
