package org.baeldung.grpc.server.dao;

import org.baeldung.grpc.server.dao.AbstractDAO;
import org.baeldung.grpc.server.model.Medication;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MedicationDAO extends AbstractDAO<Medication> {

    public List<Medication> selectAll(){
        return super.selectAll();
    }
    //se apeleaza metoda prin care se cauta id-ul unui client in campul idMedication din tabela Medication din baza de date
    public Medication findById(int id) throws SQLException{
        return super.findById(id,"idMedication");
    }
    //se adauga intr-o lista datele dorite a se insera in tabela Medication din baza de date
    public Medication insert(Integer id,String nume,Integer varsta) throws SQLException{
        ArrayList<Object> list = new ArrayList<Object>();
        list.add(id);
        list.add(nume);
        list.add(varsta);
        return super.insert(list);
    }
    //se creeaza o lista cu datele pe care dorim sa le inlocuim si o lista cu numele campurilor pe care dorim sa le inlocuim
    public Medication update(Integer newId,String newName,Integer newVarsta,int id) throws SQLException{
        ArrayList<String> names = new ArrayList<String>();
        ArrayList<Object> list = new ArrayList<Object>();
        list.add(newId);
        list.add(newName);
        list.add(newVarsta);
        names.add("idMedication");
        names.add("nume");
        names.add("varsta");
        return super.update(list,names,id,"idMedication");
    }

    public Medication delete(String name,Integer id) throws SQLException{
        ArrayList<String> names = new ArrayList<String>();
        ArrayList<Object> list = new ArrayList<Object>();

        list.add(id);
        list.add(name);
        names.add("idMedication");
        names.add("nume");

        return super.delete(list, names);
    }



}
