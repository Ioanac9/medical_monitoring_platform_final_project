package org.baeldung.grpc.server.model;

public class Prescription {

    private int id;
    private String intokeInterval;
    private String periodTreatment;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntokeInterval() {
        return intokeInterval;
    }

    public void setIntokeInterval(String intokeInterval) {
        this.intokeInterval = intokeInterval;
    }

    public String getPeriodTreatment() {
        return periodTreatment;
    }

    public void setPeriodTreatment(String periodTreatment) {
        this.periodTreatment = periodTreatment;
    }
}
