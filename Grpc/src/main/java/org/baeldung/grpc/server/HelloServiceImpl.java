package org.baeldung.grpc.server;

import org.baeldung.grpc.HelloRequest;
import org.baeldung.grpc.HelloResponse;
import org.baeldung.grpc.HelloServiceGrpc.HelloServiceImplBase;

import io.grpc.stub.StreamObserver;
import org.baeldung.grpc.server.bussinesLogic.PatientBLL;
import org.baeldung.grpc.server.dao.MedicationBD;
import org.baeldung.grpc.server.dao.PrescriptionBD;
import org.baeldung.grpc.server.model.Medication;
import org.baeldung.grpc.server.model.Patient;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class HelloServiceImpl extends HelloServiceImplBase {
    private static final Logger LOGGER = Logger.getLogger(GrpcServer.class.getName());

    public void hello(
            HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        System.out.println("Request received from client:\n" + request);


        List<Object> cnt = new ArrayList<Object>();
        List<Patient> patient1 = new ArrayList<Patient>();
        PatientBLL clientBll = new PatientBLL();


        try {
            patient1 = clientBll.selectAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String id =" ";
        String firstName = "mama mia";
        String lastName = "mama mia";
        String address = "mama mia";
        String birthDate = "mama mia";
        String gender = "mama mia";
        String medicalRecord = "mama mia";
        for(Patient it : patient1) {
            //  cnt.add(it);
           // LOGGER.info(it.getFirstName().toString());
            id = new StringBuilder()
                    .append(it.getId())
                    .toString();

            firstName = new StringBuilder()
                    .append(it.getFirstName())
                    .toString();
            lastName = new StringBuilder()
                    .append(it.getLastName())
                    .toString();
            address = new StringBuilder()
                    .append(it.getAddress())
                    .toString();
            birthDate = new StringBuilder()
                    .append(it.getBirthDate())
                    .toString();
            gender = new StringBuilder()
                    .append(it.getGender())
                    .toString();
            medicalRecord = new StringBuilder()
                    .append(it.getMedicalRecord())
                    .toString();
        }

        MedicationBD medBD= new MedicationBD();
        List<Medication> medicationList = medBD.printAllMedicationsForPatientId(Integer.parseInt(id));

        PrescriptionBD presBD = new PrescriptionBD();

//        LocalTime time = LocalTime.now();
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
//        System.out.println("Ora curenta: "+time.format(formatter));



        String medicationId = " ";
        String medicationName=" ";
        String dosage =" ";
        String sideEffects=" ";
        String prescriptionId=" ";
        String intokeInterval=" ";
        String periodTreatment=" ";
        for(Medication it : medicationList) {
            medicationId +="#"+ it.getId();
            medicationName +="#"+it.getName();
            dosage +="#"+it.getDosage();
            sideEffects+="#"+it.getSideEffects();
            prescriptionId+="#"+(presBD.printAllPrescriptionsForPatientId(it.getId()).getId());
            intokeInterval+="#"+(presBD.printAllPrescriptionsForPatientId(it.getId()).getIntokeInterval());
            periodTreatment+="#"+(presBD.printAllPrescriptionsForPatientId(it.getId()).getPeriodTreatment());
        }
        HelloResponse response = HelloResponse.newBuilder()
                .setId(id)
                .setFirstName(firstName)
                .setLastName(lastName)
                .setAddress(address)
                .setBirthDate(birthDate)
                .setGender(gender)
                .setMedicalRecord(medicalRecord)
                .setIdMedication(medicationId)
                .setMedicationName(medicationName)
                .setDosage(dosage)
                .setSideEffects(sideEffects)
                .setIdPrescription(prescriptionId)
                .setIntokeInterval(intokeInterval)
                .setPeriodTreatment(periodTreatment)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
