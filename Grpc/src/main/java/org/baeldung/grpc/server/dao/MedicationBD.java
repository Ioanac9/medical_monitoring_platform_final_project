package org.baeldung.grpc.server.dao;

import com.mysql.jdbc.Statement;
import org.baeldung.grpc.server.databaseConnection.Conexiune;
import org.baeldung.grpc.server.model.Medication;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MedicationBD {

    Statement statement = null;
    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement findStatement = null;


    private static final Logger LOGGER = Logger.getLogger(MedicationBD.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/test";
    private static final String USER = "root";


    public void printAllMedications()  {

        String query = "SELECT * FROM medication";

        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            statement = (Statement) connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String dosage = resultSet.getString("dosage");
                String name = resultSet.getString("name");
                String sideEffects= resultSet.getString("sideEffects");
                int patientId= resultSet.getInt("patientId");

                // print the results
               // LOGGER.info( id+ " "+ dosage+" "+ name+" "+ sideEffects+" "+ patientId);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }
    }

    public List<Medication> printAllMedicationsForPatientId(int idPatient )  {

        String  query = "SELECT * FROM medication where patientId = ?";
        List<Medication> medicationList = new ArrayList();

        ResultSet rs = null;

        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            findStatement = connection.prepareStatement(query);
            findStatement.setInt(1, idPatient);
            resultSet = findStatement.executeQuery();

            while (resultSet.next()) {

                Medication currentMedication = new Medication();
                int id = resultSet.getInt("id");
                String dosage = resultSet.getString("dosage");
                String name = resultSet.getString("name");
                String sideEffects= resultSet.getString("sideEffects");
                int patientId= resultSet.getInt("patientId");

                currentMedication.setId(resultSet.getInt("id"));
                currentMedication.setName(resultSet.getString("name"));
                currentMedication.setDosage(Integer.parseInt(resultSet.getString("dosage")));
                currentMedication.setSideEffects(resultSet.getString("sideEffects"));
                currentMedication.setPatientId(resultSet.getInt("patientId"));

                // print the results
             //   LOGGER.info( id+ " "+ dosage+" "+ name+" "+ sideEffects+" "+ patientId);
                medicationList.add(currentMedication);
            }

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }
        return medicationList;
    }


}
