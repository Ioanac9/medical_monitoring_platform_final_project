package org.baeldung.grpc.server.dao;

import com.mysql.jdbc.Statement;
import org.baeldung.grpc.server.databaseConnection.Conexiune;
import org.baeldung.grpc.server.model.Prescription;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class PrescriptionBD {

    Statement statement = null;
    Connection connection = null;
    ResultSet resultSet = null;
    PreparedStatement findStatement = null;


    private static final Logger LOGGER = Logger.getLogger(PrescriptionBD.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/test";
    private static final String USER = "root";




    public Prescription printAllPrescriptionsForPatientId(int medication )  {
        Prescription currentPrescription = new Prescription();
        String  query = "SELECT * FROM prescription where medication = ?";
        List<Prescription> medicationList = new ArrayList();

        ResultSet rs = null;

        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            findStatement = connection.prepareStatement(query);
            findStatement.setInt(1, medication);
            resultSet = findStatement.executeQuery();

            while (resultSet.next()) {


                int id = resultSet.getInt(medication);

                String intokeInterval = resultSet.getString("intokeInterval");
                String periodTreatment = resultSet.getString("periodTreatment");


                currentPrescription.setId(resultSet.getInt("id"));
                currentPrescription.setIntokeInterval(resultSet.getString("intokeInterval"));
                currentPrescription.setPeriodTreatment(resultSet.getString("periodTreatment"));


                // print the results
              //  LOGGER.info( id+ " "+ intokeInterval+" "+ periodTreatment+" ");

            }

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }
        return currentPrescription;
    }

    public void updateTokenPillCell(int prescriptionId,String token )  {

        String query = "update prescription set tokenPill = ? where id = ?";

        try {

            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, "");

            findStatement = connection.prepareStatement(query);
            findStatement.setString(1, token);
            findStatement.setInt(2,prescriptionId);
            findStatement.executeUpdate();

        } catch (SQLException | ClassNotFoundException e) {
            e.getCause();
        }

    }


}
