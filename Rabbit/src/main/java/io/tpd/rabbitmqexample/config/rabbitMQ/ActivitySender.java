package io.tpd.rabbitmqexample.config.rabbitMQ;

import io.tpd.rabbitmqexample.RabbitmqExampleApplication;
import io.tpd.rabbitmqexample.model.ActivityMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Service
public class ActivitySender {

    List<String> list=new ArrayList<String>();
    private static final Logger log = LoggerFactory.getLogger(ActivitySender.class);
    private final RabbitTemplate rabbitTemplate;
    // private final String fileReader = "/app/activity.txt";
    // private BufferedReader reader;

    public ActivitySender(final RabbitTemplate rabbitTemplate) throws FileNotFoundException {
        this.rabbitTemplate = rabbitTemplate;
       // reader = new BufferedReader(new FileReader(fileReader
        list.add("2011-11-28 02:27:59        2011-11-28 10:18:11     Sleeping"); 
        list.add("2011-11-28 09:21:24        2011-11-28 10:23:36     Toileting");
        list.add("2011-11-29 02:16:00        2011-11-29 16:31:00     Sleeping");
        list.add("2011-11-29 16:34:17        2011-11-29 17:08:07     Spare_Time/TV");
        list.add("2011-11-29 04:02:15        2011-11-29 20:23:38     Leaving");
        list.add("2011-12-11 14:34:12        2011-12-11 15:26:17     Lunch");

    }

    @Scheduled(fixedDelay = 1000L)
    public void sendPracticalPatient() throws IOException {
//    for (int i = 0; i < list.size(); i++) {
        

        String line = "2011-11-28 02:27:59        2011-11-28 10:18:11     Sleeping";
        if (line != null) {
            System.out.println(line);

            log.info(" Line readed before sent to RabbitMQ:  '" + line);

            String[] values = line.split("\t\t");

            ActivityMessage patient = new ActivityMessage("5c2494a3-1140-4c7a-991a-a1a2561c6bc2",
                    "2011-11-28 02:27:59","2011-11-28 17:18:11" ,  "Sleeping");
            rabbitTemplate.convertAndSend(RabbitmqExampleApplication.EXCHANGE_NAME, RabbitmqExampleApplication.ROUTING_KEY, patient);
            log.info("Activity sent to RabbitMQ " + patient.toString());
        }
    //}
}
}
